<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Quantum Reward Card | Consumer Incentives</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen"/>
    <link rel="stylesheet" type="text/css" href="css/zerogrid.css">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.transitions.css" rel="stylesheet">
    <link href="css/swiper.min.css" rel="stylesheet">
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">

    <link href="css/loader.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body  data-spy="scroll" data-target=".navbar" data-offset="50" id="index5">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-99315721-1', 'auto');
  ga('send', 'pageview');

</script>
<!--loader start-->

<!--Page Loader-->
<div class="loader">
    <div id="loader">
        <ul>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
    </div>
</div>

<!--Home start-->
<section id="home">
    <nav class="navbar navbar-fixed-top green">
        <div class="container-fluid">
            <!--second nav button -->
            <div >
                <div id="menu_bars" class="right">
                    <span class="t1"></span>
                    <span class="t2"></span>
                    <span class="t3"></span>
                </div>
            </div>
            <!-- Brand and toggle get grouped for better mobile display -->


            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><img src="images/logo-light.png" alt="logo"></a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse  ">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="#home" class="scroll">Home</a></li>
                        <li><a href="#about-us" class="scroll">About</a></li>
<!--                         <li><a href="#work" class="scroll">Our Work</a></li> -->
<!--                         <li><a href="#pricing-table-section" class="scroll">Pricing</a></li> -->
<!--                         <li><a href="#blog-section" class="scroll">News</a></li> -->
                        <li><a href="#contact-us" class="scroll">Contact Us</a></li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div>
            <div class="sidebar_menu">
                <nav class="pushmenu pushmenu-right">
                    <a class="push-logo" href="#"><img src="images/logo-dark.png" alt="logo"></a>
                    <ul class="push_nav centered">
                        <li class="clearfix">
                            <a href="#home"  class="scroll" ><span>01.</span>Home</a>

                        </li>
                        <li class="clearfix">
                            <a href="#about-us" class="scroll"> <span>02.</span>About</a>

                        </li>
<!--
                        <li class="clearfix">
                            <a href="#work" class="scroll"> <span>03.</span>Work</a>

                        </li>
-->
<!--
                        <li class="clearfix">
                            <a href="#pricing-table-section" class="scroll"> <span>04.</span>Pricing</a>

                        </li>
-->
<!--
                        <li class="clearfix">
                            <a href="#blog-section" class="scroll"> <span>05.</span>News</a>

                        </li>
-->
                        <li class="clearfix">
                            <a href="#contact-us" class="scroll"> <span>06.</span>Contact Us</a>

                        </li>
                    </ul>
                    <div class="clearfix"></div>
<!--
                    <ul class="social_icon black top25 bottom20 list-inline">

                            <li><a href="#"><i class="fa fa-fw fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-fw fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-fw fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>

                    </ul>
-->
                </nav>
            </div>
        </div>
    </nav>
   <!-- slider start -->
    <div id="slider" >
        <div class="swiper-container main-slider">
            <div class="swiper-wrapper">
                <div class="swiper-slide" style="background: url('images/cover1.jpg')">
                    <div class="container">
                        <div class="slider-content" >
                            <h2 > Custom Incentives  <br> For <span>Your Customers</span></h2>
                            <div class="buttons-group" >
                                <a href="#about-us" class="btn button  normal">Learn More</a>
                                <a href="#contact-us" class="scroll btn button special">Getting Started</a>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="swiper-slide" style="background: url('images/background-pink.jpg')">
                    <div class="container">
                        <div class="slider-content" >
                            <h2 >The ultimate reward  <br> For <span>Consumer Loyalty</span></h2>
                            <div class="buttons-group" >
                                <a href="#about-us" class="btn button  normal">Learn More</a>
                                <a href="#contact-us" class="scroll btn button special">Getting Started</a>
                            </div>

                        </div>
                    </div>
                </div>
			</div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
            <div class="swiper-button-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
        </div>
    </div>
   <!-- slider end -->
</section>
<!--Home end-->

<!-- About us Section start -->
<div id="about-us">

    <!--about-us-1 start-->
    <div  class="about-us-1 big-padding">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center ">
                    <div class="section-top-heading">
                        <h5>Streamlined Incentives</h5>
                        <h2>Visa® virtual accounts</h2>
                        <p>Visa® virtual accounts collapse the time between achievement and reward. Recipients have ultimate flexibility, and you get a solution that is secure, customizable and easy to execute.</p>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-7 wow fadeInUp" data-wow-delay="00ms">
                    <div class="outer-bound">
                        <div class="left-section">
                            <img src="images/get-virtual-reward.jpg" style="width: 400px; text-align: center;" class="img-responsive" alt="laptop">
                        </div>
                    </div>
                </div>
                <div class="col-sm-5">
                    <div class="right-section wow slideInRight animated" style="visibility: visible; animation-name: slideInRight;">
                        <div class="feature wow fadeInUp" data-wow-delay="300ms">
                            <div class="row ">

                                <div class="col-sm-2">
                                    <h2 class="icon">
                                        <i class="fa fa-life-ring" aria-hidden="true"></i>
                                        <span class="sr-only">icon</span>
                                    </h2>

                                </div>
                                <div class="col-sm-10">

                                    <h5>Support</h5>
                                    <h4>Superior 24/7/365 cardholder support available</h4>
                                    <p>Our team is available and standing by to support consumers when they need it throughout the process.</p>

                                </div>
                            </div>
                        </div>
                        <div class="feature middle wow fadeInUp" data-wow-delay="400ms">
                            <div class="row">

                                <div class="col-sm-2 ">
                                    <h2 class="icon">
                                        <i class="fa fa-leaf" aria-hidden="true"></i>
                                        <span class="sr-only">icon</span>
                                    </h2>

                                </div>
                                <div class="col-sm-10">

                                    <h5>Green</h5>
                                    <h4>Support environmental initiatives by delivering virtually</h4>
                                    <p>By utilizing our virtual delivery methods, you can help keep unnecessary waste to a minimum.</p>
                                </div>
                            </div>
                        </div>
                        <div class="feature middle wow fadeInUp" data-wow-delay="500ms">
                            <div class="row">

                                <div class="col-sm-2 ">
                                    <h2 class="icon">
                                        <i class="fa fa-flash" aria-hidden="true"></i>
                                        <span class="sr-only">icon</span>
                                    </h2>

                                </div>
                                <div class="col-sm-10">

                                    <h5>Speed</h5>
                                    <h4>Deliver rewards virtually and instantly</h4>
                                    <p>Our redemption platform allows for a near instant delivery of consumer rewards virtually via email.</p>

                                </div>
                            </div>
                        </div>
                        <div class="feature wow fadeInUp" data-wow-delay="500ms">
                            <div class="row">

                                <div class="col-sm-2 ">
                                    <h2 class="icon">
                                        <i class="fa fa-envelope" aria-hidden="true"></i>
                                        <span class="sr-only">icon</span>
                                    </h2>

                                </div>
                                <div class="col-sm-10">

                                    <h5>Virtual</h5>
                                    <h4>Create a seamless reward process</h4>
                                    <p>Utilizing our virtual delivery method allows for a faster process and more satisfied customers.</p>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--about-us-1 end-->

    <!--about-us-2 start-->
    <div  class="about-us-2 big-padding text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 ">
                    <div class="section">
                        <h3 ><i class="fa fa-quote-left" aria-hidden="true"></i>
                            Instant gratification is a powerful reward incentive
                            with Visa Virtual accounts <i class="fa fa-quote-right" aria-hidden="true"></i>
                        </h3>
<!--                         <p>- Author Name -</p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--about-us-2 end-->



</div>
<!-- About us Section end -->

<!-- services start -->
<div id="#services">


    <!-- service one start -->
    <div  class="service-1 mid-level-padding text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-center ">
                    <div class="section-top-heading">
                        <h5>Visa® virtual account</h5>
                        <h2>Features</h2>
                        <p>See what sets us apart from the competition</p>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-sm-3 wow fadeInLeft animated" style="visibility: visible;">
                    <div class="left-section section">
                        <h2><i class="fa fa-wifi" aria-hidden="true"></i><span class="sr-only">icon</span></h2>
                        <h4>Universal Visa® acceptance at merchants online</h4>
                        <hr>
                        <p>Your consumers are able to use their Visa® virtual account anywhere Visa® is accepted for online retail purchases. It's easy to use and understand.</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="middle-section section wow fadeInUp animated" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s;">
                        <h2><i class="fa fa-credit-card" aria-hidden="true"></i><span class="sr-only">icon</span></h2>
                        <h4>Plastic card option for recipients</h4>
                        <hr>
                        <p>Target your entire demographic by offering a physical card option to your consumers that can be used in-store if they would prefer.</p>
                    </div>
                </div>
                <div class="col-sm-3 wow fadeInRight animated" style="visibility: visible;">
                    <div class="middle-section section wow fadeInUp animated" data-wow-duration="2s" style="visibility: visible; animation-duration: 2s;">
                        <h2><i class="fa fa-sitemap" aria-hidden="true"></i><span class="sr-only">icon</span></h2>
                        <h4>Cardholder website</h4>
                        <hr>
                        <p>Your customers are in control with access to their cardholder website to verify balances and get information regarding their Visa® virtual account.</p>
                    </div>
                </div>
                <div class="col-sm-3 wow fadeInRight animated" style="visibility: visible;">
                    <div class="right-section section">
                        <h2><i class="fa fa-heart" aria-hidden="true"></i><span class="sr-only">icon</span></h2>
                        <h4>Custom branding</h4>
                        <hr>
                        <p>We'll delivery a custom branded product including emails and landing pages to help your customers feel right at home.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- service one end -->
</div>
<!-- services end -->

<!-- contact us section start -->
<div id="contact-us">
    <div id="contact-us2" class="mid-level-padding">
        <div class="container">

            <div class="row">
                <div class="col-sm-6">
                    <div class="left-section">
                        <div class="row">
                            <div class="col-xs-12  ">
                                <div class="section-top-heading" style="margin-bottom: 20px">
                                    <h5>Quantum Reward Card</h5>
                                    <h2>Get In Touch</h2>
                                </div>
                            </div>

                        </div>
                        <p>We are a leader in incentive based virtual and physical Visa rewards. Find out today how we can help you improve your business with incentive based Visa virtual accounts for your customers, staff, or even your leads.</p>
                        <div id="countries">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h4>United States</h4>
                                    <ul class="details one">
                                        <li><i class="fa fa-mobile" aria-hidden="true"></i>630-257-7012</li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i>info@intheq.com</li>
                                        <li><i class="fa fa-map-marker" aria-hidden="true"></i><span>12305 S New Avenue, Suite H <br>Lemont, IL 60439</span>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>
<!--
                        <ul class="list-inline">
                            <li><a href="#"><i class="fa fa-fw fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-fw fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-fw fa fa-instagram"></i></a></li>
                        </ul>
-->
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="right-section" id="form-elements">
                        <form method="post" action="mailsend.php">
                            <h4>Leave Message</h4>
                            <p>Reach out to us instantly via the form below and our staff with be in touch to discuss a customized solution.</p>
                            <?php if($_GET['send'] == 'true') : ?>
                            	<p style="background: #008110; color: #fff; border-radius: 5px; padding: 10px;">Thanks for reaching out. Your message has been sent and we will get back to you as soon as possible.</p>
                            <?php endif; ?>
                            <div class="row">
                                <div class="col-md-12 center"><div id="result"></div> </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Your Name" required="required" name="name" id="name" name="name">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email Address" required="required" name="email" id="email" name="email">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Phone No." required="required" name="phone" id="phone" name="phone">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Subject" required="required" name="subject" id="subject" name="subject">
                                    </div>
                                </div>

                            </div>

                            <div class="form-group">
                                <textarea class="form-control" placeholder="Message" required="required" id="message" name="message" name="message"></textarea>
                            </div>
                            <button type="submit" class="btn button" id="submit_btn">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>



    </div>
    <!--/g-map-->
<!--
    <div id="g-map" class="green">
        <div class="container-fluid ">
            <div class="row">
                <div id="map"></div>
            </div>
        </div>
    </div>
-->
    <!--/g-map-->
</div>
<!-- contact us section end -->

<!-- Footer-->
<footer class=" wow fadeInUp" data-wow-duration="500ms" data-wow-delay="300ms">

    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <ul class="breadcrumb">
                    <li><a href="#home" class="page-scroll scroll">Home</a></li>
                    <li><a href="#about-us" class="page-scroll scroll">About</a></li>
<!--                     <li><a href="#work" class="page-scroll scroll">Our Work</a></li> -->
<!--                     <li><a href="#pricing-table-section" class="page-scroll scroll">Pricing</a></li> -->
<!--                     <li><a href="#blog-section" class="page-scroll scroll">News</a></li> -->
                    <li><a href="#contact-us" class="page-scroll scroll">Contact Us</a></li>
                </ul>
                <p>Copyright &copy; 2017 Quantum Rewards Card. All rights reserved.</p>
            </div>
        </div>
    </div>
</footer>
<!-- /footer end -->
<!-- jQuery (necessary for Bootstrap 's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U"></script> -->
<script src="js/gmaps.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/wow.js"></script>
<script src="js/jquery.mixitup.min.js"></script>
<script type="text/javascript" src="js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="js/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="js/jquery.fancybox-thumbs.js"></script>
<script src="js/swiper.min.js" ></script>
<script src="js/jquery.appear.js"></script>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>

<script src="js/script.js"></script>
<script>

</script>
</body>

</html>
