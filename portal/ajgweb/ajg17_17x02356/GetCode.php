<?php

date_default_timezone_set('America/Chicago');

require_once('assets/php/variables.php');

require_once($Path.'connections/intheq-rewardcard.php');

require $Path.'assets/php/vendor/autoload.php';
use Mailgun\Mailgun;

require_once($Path.'assets/php/capcha/recaptchalib.php');

// Google ReCaptcha Verification
//your site secret key
$secret = '6LfygN4SAAAAAOyT1kY3Pj1j93FhnghQhxETbY4w';
//get verify response data
$verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
$responseData = json_decode($verifyResponse);

// If ReCaptcha is Correct
if($responseData->success){
    
    $forward_ip = getenv('HTTP_X_FORWARDED_FOR');
	$user_ip = getenv('REMOTE_ADDR');
	
	$code = filter_var(preg_replace('/[^A-Za-z0-9\-@_.]/', '', $_POST['enteredcode']), FILTER_SANITIZE_SPECIAL_CHARS);
	$fname = filter_var(preg_replace('/[^A-Za-z0-9\-_@.]/', '', $_POST['fname']), FILTER_SANITIZE_SPECIAL_CHARS);
	$lname = filter_var(preg_replace('/[^A-Za-z0-9\-_@.]/', '', $_POST['lname']), FILTER_SANITIZE_SPECIAL_CHARS);
	$email = strtolower(filter_var(preg_replace('/[^A-Za-z0-9\-_@.]/', '', $_POST['email']), FILTER_SANITIZE_SPECIAL_CHARS));
	$today = date('Y-m-d H:i:s');

	try {
		$stmt = $db->prepare("SELECT COUNT(*) FROM $DB_Promo WHERE Email=?");
		$stmt->bindParam(1, $email, PDO::PARAM_INT);
		$stmt->execute();

		$email_row_count = $stmt->fetchColumn();
		$stmt->closeCursor();

	} catch (PDOException $e) {
		// TODO: proper handling.
		echo "An error occurred:" . $e->getMessage();
	}
	// comment out while in test
	// As long as our user has redeemed less than three codes, redeem them now
	if ($email_row_count < $emailSecurityCheck) {
		try {
			if($activation == 'false') {
				$stmt = $db->prepare("UPDATE $DB_Promo SET Redeemed='3', Fname=?, Lname=?, Email=?, Redeem_Date=?, Activation='$today' WHERE UniqueCode=?");
			} else {
				$stmt = $db->prepare("UPDATE $DB_Promo SET Redeemed='3', Fname=?, Lname=?, Email=?, Redeem_Date=? WHERE UniqueCode=?");
			}
			$stmt->execute(array($fname, $lname, $email, $today, md5($code)));
			$stmt->closeCursor();
		} catch (PDOException $e) {
			echo "An error occurred." . $e->getMessage();
		}
	
		// Add customer to hold table
		
		if($activation == 'false' && $promoType == '0') {
			try {
				$hold = $db->prepare("INSERT INTO ".$DB_Hold." (PromoCode, Fname, Lname, Email) VALUES(:Promo, :Fname, :Lname, :Email)");
				$hold->execute(array('Promo' => $code, 'Fname' => $fname, 'Lname' => $lname, 'Email' => $email));
				$hold->closeCursor();
			} catch (PDOException $e) {
				echo "Something messed up: " . $e->getMessage();
			}
		}
		
		
		// Send hold email to customer
		// TODO - Modularize this into a function!
		
		# Instantiate the client.
		
						if($activation == 'true') {
							$auth = md5($code);
							$emailtext = "Please confirm your email address and information below and activate your prize by clicking the activate button below.";
							$activationButton = "<a href='https://".$promoHost.".com/".$URLBrand."/redeem/".$URLPromotion."/assets/php/activate.php?auth=".$auth."' class='btn' style='text-decoration:none;color:#fff;font-weight:bold;background-color:#999;padding:15px 20px;margin:5px;display:inline-block;'>Activate Your Prize</a>";
						} else {
							$emailtext = "The following information has been recorded for this redemption. You will be receiving an email in the next 2-4 hours from support@virtualrewardscenter.com to claim your gift card. You will have the option of a virtual or physical card. Please make sure to check your spam folder if you have not seen the email in 2-4 hours.";
							$activationButton = "";
						}
		
						$mgClient = new Mailgun('key-ee8ecdb878e440f14fc7d5f8e331ffc9');
						$domain = "mg.quantumrewardcard.com";
						
						if($activation == 'true') {
							$deliveryTime = date('r',strtotime("+ 2 hours"));
						} else {
							$deliveryTime = date('r',strtotime("+ 1 minutes"));
						}
						
						
						# Make the call to the client.
						$result = $mgClient->sendMessage($domain, array(
						    'from'    => "$Brand Redemption <support@quantumrewardcard.com>",
						    'to'      => $fname.' '.$lname.' <'.$email.'>',
						    'subject' => "$Brand Redemption Information",
						    'text'    => "$Brand Redemption Information",
						    'o:tag'   => array("$PreCode"),
						    'o:deliverytime' => $deliveryTime,
						    'html'    => "<html style='margin: 0;
   background-color: #575757;'>
   <head>
      <style>
         html {
         margin: 0;
         }
         body {
         margin: 0;
         font-family: arial;
         text-align: center;
         }
         a {
         text-decoration: none;
         color: #999;
         }
         h1 {
         font-size: 1.5em;
         }
         h3 {
         color: #575757;
         font-size: 22px;
         }
         #header {
         width: 100%;
         margin: 0 auto;
         background: #000;
         text-align: center;
         padding-top: 20px;
         padding-bottom: 75px;
         background: url('https://portal.$promoHost.com/".$URLBrand."/".$URLPromotion."/assets/img/email-bg.png');
         background-position: center 70%;
         background-size: cover;
         position: relative;
         }
         #header img {
         max-width: 200px;
         padding: 20px;
         position: relative;
         z-index: 10;
         }
         #container {
         width: 450px;
         margin: -60px auto;
         background: #fff;
         padding: 20px 50px 50px 50px;
         text-align: center;
         position: relative;
         z-index: 10;
         -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);
         -moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);
         box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);
         }
         #header > #content {
         color: #fff;
         width: 450px;
         padding: 0 50px;
         margin: 0 auto;
         position: relative;
         z-index: 10;
         }
         #container > #content {
         text-align: left;
         }
         #footer > p {
         font-size: 12px;
         }
         p.legal {
         font-size: 10px;
         }
         p.copyright {
         margin-top: 70px;
         color: #999;
         font-size: 12px;
         font-weight: bold;
         }
         .strong {
         font-weight: bold;
         }
         div.help {
         padding: 50px 0;
         }
         a.btn {
         color: #fff;
         font-weight: bold;
         background: #999;
         padding: 15px 20px;
         margin: 5px;
         display: inline-block;
         }
         #container > div#content > p {
         line-height: 18px;
         margin-top: 0;
         color: #575757;
         }
         #container > div#content > p.strong {
         margin-bottom: 0;
         color: #999;
         }
         #header .ov {
         width: 100%;
         height: 100%;
         position: absolute;
         background-color: rgba(0,0,0,.3);
         top: 0;
         }
      </style>
   </head>
   <body style='margin: 0;
      font-family: arial;
      text-align: center;
      background-color: #575757;'>
      <div id='header' style='width: 100%;
      margin: 0 auto;
      background-color: #000;
      text-align: center;
      padding-top: 20px;
      padding-bottom: 75px;
      background-image: url('https://portal.".$promoHost.".com/".$URLBrand."/".$URLPromotion."/assets/img/email-bg.png');
      background-size: cover;
      position: relative;'>
      <div id='content' style='color: #fff;
         width: 450px;
         padding: 20px 50px;
         margin: 0 auto;
         position: relative;
         z-index: 10;'>
         <img src='https://portal.".$promoHost.".com/".$URLBrand."/".$URLPromotion."/assets/img/logo.png' alt='Arthur J. Gallagher' style='max-width:300px;' />
         <h1 style='font-size: 1.5em;'>Congratulations!</h1>
         <p>You have successfully redeemed your promotion code for ".$PromotionArticle." ".$Promotion." courtesy of ".$Brand.".</p>
         <p class='strong' style='font-weight: bold;'>".$emailtext."</p>
      </div>
      </div>
      <table align='center'>
         <tr>
            <td>
               <div id='container' style='width: 450px;
                  margin: -60px auto;
                  background-color: #fff;
                  padding: 20px 50px 50px 50px;
                  text-align: center;
                  position: relative;
                  z-index: 10;
                  -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);
                  -moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);
                  box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);'>
                  <h3 style='color: #575757;
                     font-size: 22px;'>We have recorded the following redemption information:</h3>
                  <div id='content' style='text-align: left;'>
                     <p class='strong' style='font-weight:bold;line-height:18px;margin-top:0;color:#999;margin-bottom:0;'>First Name</p>
                     <p style='line-height: 18px;
                        margin-top: 0;
                        color: #575757;'>$fname</p>
                     <p class='strong' style='font-weight:bold;line-height:18px;margin-top:0;color:#999;margin-bottom:0;'>Last Name</p>
                     <p style='line-height: 18px;
                        margin-top: 0;
                        color: #575757;'>$lname</p>
                     <p class='strong' style='font-weight:bold;line-height:18px;margin-top:0;color:#999;margin-bottom:0;'>Email Address</p>
                     <p style='line-height: 18px;
                        margin-top: 0;
                        color: #575757;'>$email</p>
                     <p class='strong' style='font-weight:bold;line-height:18px;margin-top:0;color:#999;margin-bottom:0;'>Promotion Code</p>
                     <p style='line-height: 18px;
                        margin-top: 0;
                        color: #575757;'>$code</p>
                  </div>
                  <div class='help' style='padding: 50px 0;'>
                     <h3 style='color: #575757;
                        font-size: 22px;'>We're here to help!</h3>".
						$activationButton
                     ."<a href='https://thepromocard.com/support' class='btn' style='text-decoration:none;color:#fff;font-weight:bold;background-color:#999;padding:15px 20px;margin:5px;display:inline-block;'>Contact Support</a>
                  </div>
                  <div id='footer'>
                     <p style='font-size: 12px;'><span class='strong' style='font-weight: bold;'>This email is not your reward!</span> If you have questions, submit a customer service request. Be sure to include your name and phone number.</p>
                     <p class='legal' style='font-size:12px;'>
                        <small>".$Legal."</small>
                     </p>
                  </div>
               </div>
            </td>
         </tr>
      </table>
      <p class='copyright' style='margin-top: 70px;
         color: #999;
         font-size: 12px;
         font-weight: bold;'>Copyright © ".date('Y').", ".$promoHost.".com and it's affiliates. All rights reserved.</p>
   </body>
</html>"
));
		
		
		
		
		if($smsactive == 'true') {
			//  SEND CUSTOMER TEXT MESSAGE
			require 'assets/php/twilio-php-master/Services/Twilio.php';
							
			// Step 2: set our AccountSid and AuthToken from www.twilio.com/user/account
		    $AccountSid = "AC2f74668d43c38f30cc59ab3118ee39e5";
		    $AuthToken = "fa086b0d3b81914c9cb205c061885fad";
		 
		    // Step 3: instantiate a new Twilio Rest Client
		    $client = new Services_Twilio($AccountSid, $AuthToken);
		 
		    // Step 4: make an array of people we know, to send them a message. 
		    // Feel free to change/add your own phone number and name here.
		    $people = array(
		        "+1".$phone => $fname.' '.$lname
		    );
		 
		    // Step 5: Loop over all our friends. $number is a phone number above, and 
		    // $name is the name next to it
		    foreach ($people as $number => $name) {
			    
			    // the sms body
		            if($activation == 'true') {
						$smsmessage = "Congratulations ".$fname."! You have successfully redeemed your promotion code. You'll get an activation link soon!";
					} else {
						$smsmessage = "Congratulations ".$fname."! You have successfully redeemed your promotion code. You'll get a confirmation email soon!";
					}

		 
		        $sms = $client->account->messages->sendMessage(
		 
		        // Step 6: Change the 'From' number below to be a valid Twilio number 
		        // that you've purchased, or the (deprecated) Sandbox number
		            "831-331-4614",
		 
		            // the number we are sending to - Any phone number
		            $number, $smsmessage
		 
		            		            
		        );
		 
		        // Display a confirmation message on the screen
		        //echo "Sent message to $name";
		    }
		}
		

		// Redirect to redemption complete screen. 
		header("Location: redeemcomplete.php?fn=$fname&ln=$lname&em=$email&code=$code");

	} else {
		// TODO: Add verbiage for when a user has already redeemed three codes. 
		header('Location: index.php?error=3');
	} 

// If ReCaptcha is Incorrect 
} else {
	header("Location: index.php?capcha=failed&Enteredcode=$code&Desc=$desc&fname=$fname&lname=$lname&email=$email");
}


