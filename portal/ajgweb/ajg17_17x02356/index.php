<?php

require_once('assets/php/variables.php');	

require_once($Path.'connections/intheq-rewardcard.php');

//PROMOTION END//
date_default_timezone_set('America/Chicago');

if(time() < strtotime($PromoStart) && (substr($_GET['Code'], 5, 4) != 'TEST')) {
     header("location: comingsoon.php");
}

else if(time() > strtotime($PromoEnd) && (substr($_GET['Code'], 5, 4) != 'TEST')) {
	header("location: expired.php");
}

// include("assets/php/legal.html");

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$today = date('Y-m-d');

$FormCode_CurrentCode = "-1";
if (isset($_GET['Code'])) {
  $FormCode_CurrentCode = $_GET['Code'];
}

$FormCode2_CurrentCode = "-1";
if (isset($_GET['auth'])) {
  $FormCode2_CurrentCode = $_GET['auth'];
}

try {
    $stmt = $db->prepare("SELECT * FROM $DB_Promo WHERE (UniqueCode = md5(?)) OR (UniqueCode = ?)");
    $stmt->bindParam(1, $FormCode_CurrentCode, PDO::PARAM_STR);
    $stmt->bindParam(2, $FormCode2_CurrentCode, PDO::PARAM_STR);
    $stmt->execute();
    // set the resulting array to associative
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $row_CurrentCode = $stmt->fetch();
    $totalRows_CurrentCode = $stmt->rowCount();
    $_GET['Code'] = $row_CurrentCode['UC2'];
}
catch(PDOException $e) {
    echo "Error: " . $e->getMessage();
}
$db = null;


?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?php echo $promoHost; ?>.com</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

 <!-- Le styles -->
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
  <link href='//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link id="elemento-theme" href= "<?php echo $Path ?>assets/css/bootstrap.fire.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="assets/css/bootstrap-material-design.min.css">
  <link id="elemento-theme" href="assets/css/main.css" rel="stylesheet">
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
  <script src="assets/js/jvfloat.min.js"></script>
  <link id="jvfloat" href="assets/css/jvfloat.css" rel="stylesheet">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  
  
  

<script src="assets/js/material.min.js"></script>
<script src="assets/js/moment-with-locales.min"></script>
  
  
  
  <script src='https://www.google.com/recaptcha/api.js'></script>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $Path ?>assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $Path ?>assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $Path ?>assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo $Path ?>assets/ico/apple-touch-icon-57-precomposed.png">

  <script type="text/javascript" language="JavaScript">
  <!--
  //--------------------------------
  // This code compares two fields in a form and submit it
  // if they're the same, or not if they're different.
  //--------------------------------
  function validateForm( )
  {
  valid = true;

  if ( document.promoValidate.fname.value == "" )
  {
      $("#fnamediv").addClass("error");
    	document.getElementById("fnameerror").style.display = "";
      valid = false;
      window.scrollTo(0, 0);
  }

  if ( document.promoValidate.lname.value == "" )
  {
      $("#lnamediv").addClass("error");
    	document.getElementById("lnameerror").style.display = "";
      valid = false;
      window.scrollTo(0, 0);
  }

  if ( document.promoValidate.email.value == "" )
  {
      $("#emaildiv").addClass("error");
    	document.getElementById("emailerror").style.display = "";
      valid = false;
      window.scrollTo(0, 0);
  }

  var x=document.forms["promoValidate"]["email"].value;
  var atpos=x.indexOf("@");
  var dotpos=x.lastIndexOf(".");
  if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length)
    {
    $("#emaildiv").addClass("error");
    document.getElementById("emailerror").style.display = "";
    valid = false;
    window.scrollTo(0, 0);
   }

  var email = document.getElementById("email").value
  var confEmail = document.getElementById("confirmemail").value
  if(email != confEmail) {
  	$("#confirmemaildiv").addClass("error");
    document.getElementById("confirmemailerror").style.display = "";
    valid = false;
    window.scrollTo(0, 0);
  }


  return valid;
  }

  //-->
  </script>
</head>

<body>

<div class="container-fluid heading">
	<div class="row">
		<div class="col-md-12">
			<?php if (file_exists('../assets/img/logo.png')) : ?>
				<img src="../assets/img/logo.png" alt="<?php echo $promoHost; ?>.com">
			<?php else : ?>
				<img src="<?php echo $Path ?>assets/img/logo.png" alt="<?php echo $promoHost; ?>.com">
			<?php endif; ?>
		</div>
	</div>
</div>


<div class="container-fluid content">

  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-6 left">
	    <h1><?php echo $brandIntroHeading; ?></h1>
		<p><?php echo $brandIntroText; ?></p>
    </div>
    <div class="col-md-6 right">
	    <?php if(isset($row_CurrentCode['Redeemed']) && ($row_CurrentCode['Redeemed'] == '2' || $row_CurrentCode['Redeemed'] == '3' || $row_CurrentCode['Redeemed'] == '4')) { ?>
		    <table border="0" class="span5 offset1">
		        <tr>
		          <td><br><br><br><br>
		      <center><div class='alert alert-warning theme fade in' style='display: block;'>
					<table width='90%' border='0'><tr><td width='16px' valign='top'>
					  <i class='icon-warning-sign'></i></td>
		    <td><strong>Well this isn't good...</strong> It seems you have already redeemed your promotion code.</td></tr></table></div></center>
		    </td>
		    </tr>
		    </table>
		<?php } else if(isset($row_CurrentCode['Expire']) && ($row_CurrentCode['Expire'] < date('Y-m-d'))) { ?>
		    <table border="0" class="span5 offset1">
		        <tr>
		          <td><br><br><br><br>
		      <center><div class='alert alert-warning theme fade in' style='display: block;'>
					<table width='90%' border='0'><tr><td width='16px' valign='top'>
					  <i class='icon-warning-sign'></i></td>
		    <td><strong>Well this isn't good...</strong> Looks like your code is expired! Sorry but per the terms of this promotion, you may no longer redeem your code.</td></tr></table></div></center>
		    </td>
		    </tr>
		    </table>
		<?php } else { ?>
			<?php if ($totalRows_CurrentCode == 0) { // Show if recordset empty ?>

                  <?php print "<script language='Javascript'>document.location.href='http://www.".$promoHost.".com/".$URLBrand."/redeem/index.php?error=1' ;</script>"; ?>

            <?php } // Show if recordset empty ?>
			<?php if ($totalRows_CurrentCode > 0) { // Show if recordset not empty ?>
			
			
				<center>
		        	<span class="label label-warning theme" style="font-size:x-large; padding:10px;">Step 2</span>
		        </center>
		        <div class="progress progress-warning progress-striped active" style="display:block;">
		        	<div class="bar theme" style="width: 66%;">Step 2 of 3</div>
		        </div>
		        <center>
		        	<h4 style="display: block;">Enter the information below to process and receive your <?php echo $Promotion ?> courtesy of <?php echo $Brand ?>.</h4>
		        	<span class="label-warning label theme">All fields are required!</span>
		        </center>
		        <?php if (isset($_GET['capcha']) && $_GET['capcha'] == 'failed') : ?>
					<center>
						<div class='alert alert-error theme fade in'>
							<button type='button' class='close' data-dismiss='alert'>&times;</button>
					        <table width='90%' border='0'>
						        <tr>
							        <td width='16px' valign='top'>
										<i class='icon-warning-sign'></i>
									</td>
									<td>
										<strong>Uh Oh!</strong> Our system thinks you're a spam robot. Please try again.
									</td>
								</tr>
							</table>
						</div>
					</center>
				<?php endif; ?>
				
				<div class='alert alert-error theme fade in' id="confirmemailerror" style="display:none;">
		            <table width='90%' border='0'>
		              <tr>
		                <td width='16px' valign='top'><i class='icon-warning-sign'></i></td>
		                <td><strong>Oh No!</strong> Your email addresses do not match!</td>
		              </tr>
		            </table>
		        </div>
		        <div class='alert alert-error theme fade in' id="emailerror" style="display:none;">
		            <table width='90%' border='0'>
		              <tr>
		                <td width='16px' valign='top'><i class='icon-warning-sign'></i></td>
		                <td><strong>Sorry,</strong> Your email address appears to be missing or an invalid format!</td>
		              </tr>
		            </table>
		        </div>
		        <div class='alert alert-error theme fade in' id="fnameerror" style="display:none;">
		            <table width='90%' border='0'>
		              <tr>
		                <td width='16px' valign='top'><i class='icon-warning-sign'></i></td>
		                <td><strong>Yikes!</strong> Your First Name is required.</td>
		              </tr>
		            </table>
		        </div>
		        <div class='alert alert-error theme fade in' id="lnameerror" style="display:none;">
		            <table width='90%' border='0'>
		              <tr>
		                <td width='16px' valign='top'><i class='icon-warning-sign'></i></td>
		                <td><strong>Oh boy...</strong> Your Last Name is required.</td>
		              </tr>
		            </table>
		        </div>
		        
				
				
				<form id="promoValidate" name="promoValidate" action="GetCode.php?Code=<?php echo $_GET['Code']; ?>" onsubmit="return validateForm()" method="POST" enctype="multipart/form-data">
					<fieldset>
						<div id="fnamediv" class="control-group">
							
							<div class="controls">
								<input class="forminput" name="fname" id="fname" type="text" placeholder="First Name" <?php if(isset($_GET['captcha']) && $_GET['captcha'] == 'failed') { ?>value="<?php echo $_GET['fname'] ?>"<?php } ?>>
							</div>
						</div>
						<div id="lnamediv" class="control-group">
							
							<div class="controls">
								<input class="forminput" name="lname" id="lname" type="text" placeholder="Last Name" <?php if(isset($_GET['captcha']) && $_GET['captcha'] == 'failed') { ?>value="<?php echo $_GET['lname'] ?>"<?php } ?>>
							</div>
						</div>
						<div id="emaildiv" class="control-group">
							
							<div class="controls">
								<input class="forminput" name="email" id="email" type="email" placeholder="Email Address" <?php if(isset($_GET['captcha']) && $_GET['captcha'] == 'failed') { ?>value="<?php echo $_GET['email'] ?>"<?php } ?>>
							</div>
						</div>
						<div id="confirmemaildiv" class="control-group">
							
							<div class="controls">
								<input class="forminput" name="confirmemail" id="confirmemail" type="email" placeholder="Confirm Email Address" <?php if(isset($_GET['captcha']) && $_GET['captcha'] == 'failed') { ?>value="<?php echo $_GET['email'] ?>"<?php } ?>>
							</div>
						</div>
						<br /><br />
						<div class="g-recaptcha" data-sitekey="6LfygN4SAAAAANN26knbd2YruR-M9QGJ101yT-Wn"></div>
						<br>
						<br>
						<input name="enteredcode" type="hidden" id="enteredcode" value="<?php echo $_GET['Code']; ?>" />
						<input name="redeemed" type="hidden" id="redeemed" value="2" />
						<input name="Desc" type="hidden" id="Desc" value="<?php echo $row_PromoDesc['Description']; ?><?php echo $_GET['Desc']; ?>" />
						<input name="PromoID" type="hidden" id="PromoID" value="<?php echo $row_CurrentCode['Promo_ID']; ?>" />
						<button class="btn btn-warning theme" id="submitbutton" name="submitbutton" type="submit">Submit</button>
					</fieldset>
				</form>
			
			
			<?php } // Show if recordset not empty ?></td>
		<?php } ?>
	    
	    
	    
	    
	    
	    
	    
	    
	
	</div>
  </div>
  <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<footer>
			    <p>&copy; <?php echo $promoHost; ?>.com <?php date('Y'); ?>. All Rights Reserved. </p>
			    <p>
				    <a href="<?php echo $Path ?>FAQ.php">Frequently Asked Questions</a> | 
				    <a href="https://<?php echo $promoHost; ?>.zendesk.com/hc/en-us">Contact Support</a>
				    <br>
					Use of this website constitutes your acceptance of our <a href="<?php echo $Path ?>tos.php">Terms of Service</a> and <a href="<?php echo $Path ?>pp.php">Privacy Policy</a>.
					<br>
					<small><?php echo $Legal; ?></small>
			    </p>
			</footer>
		</div>
	</div>

</div>
<!-- /container -->

<!-- Le javascript
    ================================================== -->
<script>
	
	
	(function($) {
  $.fn.placeholder = function() {
    if(typeof document.createElement("input").placeholder == 'undefined') {
      $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
          input.removeClass('placeholder');
        }
      }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
          input.addClass('placeholder');
          input.val(input.attr('placeholder'));
        }
      }).blur().parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
          var input = $(this);
          if (input.val() == input.attr('placeholder')) {
            input.val('');
          }
      })
    });
  }
}
})(jQuery);
	
jQuery.fn.placeholder();
	
	
	jQuery(function() {
// 	    if ( $('#pdate')[0].type != 'date' ) $('#pdate').datepicker();
		var today = new Date();
		$('#pdate').bootstrapMaterialDatePicker({ weekStart : 0, time: false, maxDate : today });
	  });
	
	function helpText()
	{
	
	if($('#poplabel span').text().length == 0)
	{
		console.log($('#poplabel span').text().length)
      $("#popdiv .help").slideDown();
  	} else {
	  	console.log($('#poplabel span').text().length)
	  $("#popdiv .help").slideUp(); 	
  	}
	
  	}
  		
	jQuery(document).ready(function() {
	    jQuery("#phone").keydown(function (e) {
	        // Allow: backspace, delete, tab, escape, enter and .
	        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
	             // Allow: Ctrl+A, Command+A
	            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
	             // Allow: home, end, left, right, down, up
	            (e.keyCode >= 35 && e.keyCode <= 40)) {
	                 // let it happen, don't do anything
	                 return;
	        }
	        // Ensure that it is a number and stop the keypress
	        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	            e.preventDefault();
	        }
	    });
	});
	
	jQuery(function() {
	    jQuery('.forminput').jvFloat();
	});
	
	/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
	*/
	
	'use strict';
	
	;( function ( document, window, index )
	{
		var inputs = document.querySelectorAll( '.inputfile' );
		Array.prototype.forEach.call( inputs, function( input )
		{
			var label	 = input.nextElementSibling,
				labelVal = label.innerHTML;
	
			input.addEventListener( 'change', function( e )
			{
				var fileName = '';
				if( this.files && this.files.length > 1 )
					fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
				else
					fileName = e.target.value.split( '\\' ).pop();
	
				if( fileName )
					label.querySelector( 'span' ).innerHTML = fileName;
				else
					label.innerHTML = labelVal;
			});
	
			// Firefox bug fix
			input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
			input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
		});
	}( document, window, 0 ));
	
</script>
<!-- Placed at the end of the document so the pages load faster -->
<!-- Start of redeemyouroffers Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","redeemyouroffers.zendesk.com");
/*]]>*/</script>
<!-- End of redeemyouroffers Zendesk Widget script -->


</body>
</html>