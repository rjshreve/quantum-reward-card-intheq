<?php

    require_once('../../../../../Connections/intheq-int.php');
    require_once('variables.php');
    
    require $Path.'../../assets/php/vendor/autoload.php';
	use Mailgun\Mailgun;

    echo "Requires loaded. \n";
    mysql_select_db($database_intheq, $intheq);

    echo "Database selected.";


    joinDateFilter($Promotion,$Brand,$PreCode);

        // WHOSE AD IS 30 DAYS OLD
        // What does this comment mean? Not sure. SDW.
        function joinDateFilter($Promotion,$Brand,$PreCode){
            $query = mysql_query("SELECT * FROM Hold_Emails WHERE sent IS NULL AND ready='1'");
			$sendarray = array();
			$emaillist = array();
			
			
            while ($row = mysql_fetch_array($query, MYSQL_ASSOC)){

				$mail_to = $row['email'];
                $time = date('Y-m-d H:m:s');
				$id = $row["id"];
				
				$emaillist[] = $row['email'];
				$sendarray[$row['email']] = $row;
				
				mysql_query("UPDATE Hold_Emails SET sent='".$time."' WHERE id='".$id."'");
				
            }
			
			sendEmail($sendarray,$emaillist,$Brand,$Promotion,$PreCode);
			
			echo "Emails Sent!";
        }

    // SEND EMAIL
        function sendEmail($sendarray,$emaillist,$Brand,$Promotion,$PreCode) {
            $Brand = "$Brand";
			$Promotion = "$Promotion";
			$PreCode = "$PreCode";
			$sendarrayjson = json_encode($sendarray);
			$sendlistcomma = implode (", ", $emaillist);
			$vararray = '{"prize": "%recipient.code1%" , "name": "'.date('Y-m-d').'_EmailCodes" , "promo": "%recipient.promo%" }';	
            
                //begin of HTML message
			
						$mgClient = new Mailgun('key-ee8ecdb878e440f14fc7d5f8e331ffc9');
						$domain = "mg.thepromocard.com";
						
						# Make the call to the client.
						$result = $mgClient->sendMessage($domain, array(
						    'from'    => 'Office Depot Redemption <support@thepromocard.com>',
						    'to'      => "$sendlistcomma",
						    'subject' => 'Office Depot Promotion Delivery',
						    'text'    => 'Office Depot Promotion Delivery',
						    'recipient-variables' => "$sendarrayjson",
						    'v:batch_info' => "$vararray",
						    'o:tag'   => array("$PreCode", "BATCH", "Bulk_".date('Y-m-d')),
						    'html'    => "<html style='margin: 0;
							   background-color: #575757;'>
							   <head>
							      <style>
							         html {
							         margin: 0;
							         }
							         body {
							         margin: 0;
							         font-family: arial;
							         text-align: center;
							         }
							         a {
							         text-decoration: none;
							         color: #999;
							         }
							         h1 {
							         font-size: 1.5em;
							         }
							         h3 {
							         color: #575757;
							         font-size: 22px;
							         }
							         #header {
							         width: 100%;
							         margin: 0 auto;
							         background: #000;
							         text-align: center;
							         padding-top: 20px;
							         padding-bottom: 75px;
							         background: url('https://thepromocard.com/officedepot/redeem/assets/img/email-bg.jpeg');
							         background-position: center 70%;
							         background-size: cover;
							         position: relative;
							         }
							         #header img {
							         max-width: 200px;
							         padding: 20px;
							         position: relative;
							         z-index: 10;
							         }
							         #container {
							         width: 450px;
							         margin: -60px auto;
							         background: #fff;
							         padding: 20px 50px 50px 50px;
							         text-align: center;
							         position: relative;
							         z-index: 10;
							         -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);
							         -moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);
							         box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);
							         }
							         #header > #content {
							         color: #fff;
							         width: 450px;
							         padding: 0 50px;
							         margin: 0 auto;
							         position: relative;
							         z-index: 10;
							         }
							         #container > #content {
							         text-align: left;
							         }
							         #footer > p {
							         font-size: 12px;
							         }
							         p.legal {
							         font-size: 10px;
							         }
							         p.copyright {
							         margin-top: 70px;
							         color: #999;
							         font-size: 12px;
							         font-weight: bold;
							         }
							         .strong {
							         font-weight: bold;
							         }
							         div.help {
							         padding: 50px 0;
							         }
							         a.btn {
							         color: #fff;
							         font-weight: bold;
							         background: #999;
							         padding: 15px 20px;
							         margin: 5px;
							         display: inline-block;
							         }
							         #container > div#content > p {
							         line-height: 18px;
							         margin-top: 0;
							         color: #575757;
							         }
							         #container > div#content > p.strong {
							         margin-bottom: 0;
							         color: #999;
							         }
							         #header .ov {
							         width: 100%;
							         height: 100%;
							         position: absolute;
							         background-color: rgba(0,0,0,.3);
							         top: 0;
							         }
							      </style>
							   </head>
							   <body style='margin: 0;
							      font-family: arial;
							      text-align: center;
							      background-color: #575757;'>
							      <div id='header' style='width: 100%;
							      margin: 0 auto;
							      background-color: #000;
							      text-align: center;
							      padding-top: 20px;
							      padding-bottom: 75px;
							      background-image: url('https://thepromocard.com/officedepot/redeem/assets/img/email-bg.jpeg');
							      background-size: cover;
							      position: relative;'>
							      <div id='content' style='color: #fff;
							         width: 450px;
							         padding: 20px 50px;
							         margin: 0 auto;
							         position: relative;
							         z-index: 10;'>
							         <h1 style='font-size: 1.5em;'>Congratulations!</h1>
							         <p>You have successfully redeemed your promotion code for $Promotion courtesy of $Brand.</p>
							         <p class='strong' style='font-weight: bold;'>Please click the 'Print/View Certificate' button below for your reward.</p>
							      </div>
							      </div>
							      <table align='center'>
							         <tr>
							            <td>
							               <div id='container' style='width: 450px;
							                  margin: -60px auto;
							                  background-color: #fff;
							                  padding: 20px 50px 50px 50px;
							                  text-align: center;
							                  position: relative;
							                  z-index: 10;
							                  -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);
							                  -moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);
							                  box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.5);'>
							                  <h3 style='color: #575757;
							                     font-size: 22px;'>We have recorded the following redemption information:</h3>
							                  <div id='content' style='text-align: left;'>
							                     <p class='strong' style='font-weight:bold;line-height:18px;margin-top:0;color:#999;margin-bottom:0;'>First Name</p>
							                     <p style='line-height: 18px;
							                        margin-top: 0;
							                        color: #575757;'>%recipient.fname%</p>
							                     <p class='strong' style='font-weight:bold;line-height:18px;margin-top:0;color:#999;margin-bottom:0;'>Last Name</p>
							                     <p style='line-height: 18px;
							                        margin-top: 0;
							                        color: #575757;'>%recipient.lname%</p>
							                     <p class='strong' style='font-weight:bold;line-height:18px;margin-top:0;color:#999;margin-bottom:0;'>Email Address</p>
							                     <p style='line-height: 18px;
							                        margin-top: 0;
							                        color: #575757;'>%recipient.email%</p>
							                     <p class='strong' style='font-weight:bold;line-height:18px;margin-top:0;color:#999;margin-bottom:0;'>Promotion Code</p>
							                     <p style='line-height: 18px;
							                        margin-top: 0;
							                        color: #575757;'>%recipient.promo%</p>
							                  </div>
							                  <div class='help' style='padding: 50px 0 20px;'>
							                     <h3 style='color: #575757;
							                        font-size: 22px;'>Claim Your Prize!</h3>
							                        <a href='https://thepromocard.com/officedepot/redeem/ODFAN_17x01067/assets/php/redirect.php?fname=%recipient.fname%&lname=%recipient.lname%&email=%recipient.email%&promo=%recipient.promo%&prize=%recipient.code1%&pin=%recipient.code2%' class='btn' style='text-decoration:none;color:#fff;font-weight:bold;background-color:#999;padding:15px 20px;margin:5px;display:inline-block;'>View/Print Certificate</a>
							                  </div>
							                  <div class='help' style='padding: 20px 0 50px;'>
							                     <h3 style='color: #575757;
							                        font-size: 22px;'>We're here to help!</h3>
							                        <a href='https://thepromocard.com/support' class='btn' style='text-decoration:none;color:#fff;font-weight:bold;background-color:#999;padding:15px 20px;margin:5px;display:inline-block;'>Contact Support</a>
							                  </div>
							                  <div id='footer'>
							                     <p style='font-size: 12px;'><span class='strong' style='font-weight: bold;'>This email is not your reward!</span> If you have questions, submit a customer service request. Be sure to include your name and phone number.</p>
							                     <p class='legal' style='font-size:12px;'>
							                        <small>Movie Certificate Terms and Conditions: Movie certificate is good towards two admissions (up to $24 total value) to see any movie at participating theaters in the U.S. Reward code void if not activated by 4/30/2017, and certificate void if not used within 2 months from the date of online code activation. If lost or stolen, cannot be replaced. No reproductions will be accepted. No cash value. No change will be provided. Internet access and printer required, and must bring printed certificate to the participating theater box office to redeem. Not valid with any other offer. Offer valid for one-time use only. Not for resale; void if sold or exchanged. If cost of movie admissions is more than maximum value of the movie certificate, then user must pay the difference. Limit 1 rewards per person and/or business. Fandango Loyalty Solutions, LLC is not a sponsor or co-sponsor of this program. The redemption of the reward code is subject to Fandango’s Terms and Policies at <a href='http://www.fandango.com/policies/terms-and-policies' target='_blank'>www.fandango.com/policies/terms-and-policies</a>. Use of the movie certificate is subject to the terms and conditions at <a href='http://www.activaterewards.com/hmm/terms' target='_blank'>www.activaterewards.com/hmm/terms</a>. All Rights Reserved.</small>
							                     </p>
							                  </div>
							               </div>
							            </td>
							         </tr>
							      </table>
							      <p class='copyright' style='margin-top: 70px;
							         color: #999;
							         font-size: 12px;
							         font-weight: bold;'>Copyright © ".date('Y').", ThePromoCard.com and it's affiliates. All rights reserved.</p>
							   </body>
							</html>"
							));
			
		}
    ?>