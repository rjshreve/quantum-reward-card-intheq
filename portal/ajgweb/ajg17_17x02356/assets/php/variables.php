<?php

// Promotion Type (0 = Internal / 1 = External)
$promoType = '1';

// Promotion Host (ThePromoCard / RedeemYourOffers)
$promoHost = 'QuantumRewardCard';

// START Database Information
$DB_Promo = "AJG17_Promo";
$DB_Hold = "AJG17_Hold";

// Virtual Incentives Info for External Promos
$VI_ProgID = "46394";
$VI_itemID = "AJG17";
$VI_itemPrice = "100";
$VI_SKUID = "UVC-V-R18";

$brandIntroHeading = "Thank you for your participation!";
$brandIntroText = "Please complete the redemption process to confirm your redemption and finalize your reward.";

$smsactive = 'false';
$activation = 'false';
$emailSecurityCheck = '3';

// Promotion-specific variables

// Day site opens for redemptions
$PromoStart = "2017/04/18";

// Last day of redemptions
// CODES WILL NOT WORK at 12:00AM ON THIS DATE
$PromoEnd = "2017/09/30";

// Site shuts off
$PromoOff = "2017/10/30";

// This is silly, but needs to stay for the moment.
$Path = "../../core/";

$PreCode = "ajg17";

// Used to direct users to the proper brand's gateway
$URLBrand = "ajgweb";

// Specific portal for promotion
$URLPromotion = $PreCode."_17x02356";

$PromotionArticle = "a";
// Verbose prize for current promotion
$Promotion = "$100 Prepaid Visa&reg;";

// Verbose brands, used in communication with customers
$Brand = "Arthur J. Gallagher & Co.";
$PrizeBrand = "Visa";

$Theme = "8fc3ea";

$Legal = "This Prepaid Visa is issued by Sutton Bank, member FDIC, pursuant to license by Visa U.S.A. Inc. Card powered by Marqeta.";

?>
