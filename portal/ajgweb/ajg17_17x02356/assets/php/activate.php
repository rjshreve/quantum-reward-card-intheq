<?php

	require_once('variables.php');
	
	$db = new PDO('mysql:host=localhost;dbname=intheqPromo-INT', $dbuser, $dbpw);
	
	// Get prize wheel POST variables
	$secureID = $_GET["auth"];
	$currentdate = date('Y-m-d H:i:s');
	
	try {
		$stmt = $db->prepare("SELECT * FROM ODFAN_Promo WHERE UniqueCode=? LIMIT 1");
		$stmt->bindParam(1, $secureID, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
			
	} catch (PDOException $e) {
		// TODO: proper handling.
		echo "An error occurred:" . $e->getMessage();
	}
	// comment out while in test
	// As long as our user has redeemed less than three codes, redeem them now
	
	if ($row['Activation'] == NULL) {
	
		try {
			$stmt = $db->prepare("UPDATE $DB_Promo SET Activation=? WHERE UniqueCode=?");
			$stmt->bindParam(1, $currentdate, PDO::PARAM_STR);
			$stmt->bindParam(2, $secureID, PDO::PARAM_STR);
			$stmt->execute();
			$new_row = $stmt->fetchColumn();
			$stmt->closeCursor();
			
			$holdstmt = $db->prepare("INSERT INTO $DB_Hold ( PromoCode, Fname, Lname, Email ) VALUES ( ?,?,?,? )");
			$holdstmt->bindParam(1, $row['UC2'], PDO::PARAM_STR);
			$holdstmt->bindParam(2, $row['Fname'], PDO::PARAM_STR);
			$holdstmt->bindParam(3, $row['Lname'], PDO::PARAM_STR);
			$holdstmt->bindParam(4, $row['Email'], PDO::PARAM_STR);
			$holdstmt->execute();
			$holdstmt->closeCursor();
			
			header("Location: ../../activatecomplete.php?auth=$secureID");
		
		} catch (PDOException $e) {
			// TODO: proper handling.
			echo "An error occurred:" . $e->getMessage();
		}
	} else {
		$activationDate = $row['Activation'];
		header("Location: ../../activatecomplete.php?auth=$secureID&activation=$activationDate");
	}

?>