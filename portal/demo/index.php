<?php
	
header('location: code_direct.php?Code=DEMO1-55555-55555');
exit;

require_once('assets/php/variables.php');
require_once($Path.'connections/intheq-rewardcard.php');
// include("assets/php/legal.html");

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$today = date('Y-m-d');

$FormCode_CurrentCode = "-1";
if (isset($_GET['Code'])) {
  $FormCode_CurrentCode = $_GET['Code'];
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>QuantumRewardCard.com</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
  <link href='//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link id="elemento-theme" href= "<?php echo $Path ?>assets/css/bootstrap.fire.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="assets/css/bootstrap-material-design.min.css">
  <link id="elemento-theme" href="assets/css/main.css" rel="stylesheet">
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
  <script src="assets/js/jvfloat.min.js"></script>
  <link id="jvfloat" href="assets/css/jvfloat.css" rel="stylesheet">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>




<script src="assets/js/material.min.js"></script>
<script src="assets/js/moment-with-locales.min"></script>


  <script src='https://www.google.com/recaptcha/api.js'></script>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $Path ?>assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $Path ?>assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $Path ?>assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo $Path ?>assets/ico/apple-touch-icon-57-precomposed.png">
</head>

<body>

<div class="container-fluid heading">
	<div class="row">
		<div class="col-md-12">
			<img src="assets/img/logo.png" alt="QuantumRewardCard.com">
		</div>
	</div>
</div>


<div class="container-fluid content">

  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-6 left">
	    <h1><?php echo $brandIntroHeading; ?></h1>
		<p><?php echo $brandIntroText; ?></p>
    </div>
    <div class="col-md-6 right">
	    <center>
        	<span class="label label-warning theme" style="font-size:x-large; padding:10px;">Step 1</span>
        </center>
        <div class="progress progress-warning progress-striped active" style="display:block;">
        	<div class="bar theme" style="width: 33%;">Step 1 of 3</div>
        </div>

        <?php
if (isset($_GET['error']) && $_GET['error'] == '1') { ?>
   <br><center><div class='alert alert-warning theme fade in' style='display: block;'>

              <table width='90%' border='0'><tr><td width='16px' valign='top'>
			  <i class='icon-warning-sign'></i></td>
    <td><strong>Well this isn't good!</strong> The Code you entered doesn't seem to match any of the available offers. Please verify your code and try again. If you continue to experience errors, please <a href="https://RedeemYourOffers.zendesk.com/hc/en-us">Contact Customer Support</a>.</td></tr></table></div></center><br>
<?php } ?>
<?php
if (isset($_GET['error']) && $_GET['error'] == '2') { ?>
   <br><center><div class='alert alert-error fade in' style='display: block;'>

              <table width='90%' border='0'><tr><td width='16px' valign='top'>
			  <i class='icon-warning-sign'></i></td>
    <td><strong>We'll take the blame!</strong> An error has occured in determining your prize. Please try again. If you continue to experience errors, <a href="https://RedeemYourOffers.zendesk.com/hc/en-us">Contact Customer Support</a>.</td></tr></table></div></center><br>
<?php } ?>
<?php
if (isset($_GET['error']) && $_GET['error'] == '3') { ?>
   <br><center><div class='alert alert-error fade in' style='display: block;'>

              <table width='90%' border='0'><tr><td width='16px' valign='top'>
			  <i class='icon-warning-sign'></i></td>
    <td><strong>For security reasons</strong> your redemption has been flagged. Please contact <a href="https://RedeemYourOffers.zendesk.com/hc/en-us">Customer Support</a> and reference error code <strong>DF345F-46</strong></td></tr></table></div></center><br>
<?php } ?>


        <center>
        	<h4>Validate your Access Code below.</h4>
        </center>

		<form name="promoValidate" method="post" action="code_direct.php" style="width: 100%;">
          <div class="input-append text-center" style="width: 100%;">
            <input class="col-md-12 text-center" name="PromoCode" id="appendedInputButton" type="text" placeholder="Access Code" rel="popover" data-placement="bottom" data-content="Please enter your Access Code EXACTLY as it appears on your instruction sheet." title="Tips">
            <button class="btn btn-warning theme" type="submit">Validate!</button>
          </div>
        </form>

	</div>
  </div>
  <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<footer>
			    <p>&copy;<?php echo date('Y'); ?> QuantumRewardCard.com. All Rights Reserved. </p>
			    <p>
				    <a href="<?php echo $Path ?>FAQ.php">Frequently Asked Questions</a> |
				    <a href="https://RedeemYourOffers.zendesk.com/hc/en-us">Contact Support</a>
				    <br>
					Use of this website constitutes your acceptance of our <a href="<?php echo $Path ?>tos.php">Terms of Service</a> and <a href="<?php echo $Path ?>pp.php">Privacy Policy</a>.
					<br>
			    </p>
			</footer>
		</div>
	</div>

</div>
<!-- /container -->

<!-- Le javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

</body>
</html>
