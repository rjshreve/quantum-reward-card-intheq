<!DOCTYPE html>


<?php

require_once('assets/php/variables.php');
require_once($Path.'connections/intheq-rewardcard.php');

//PROMOTION END//
date_default_timezone_set('America/Chicago');

if(time() < strtotime($PromoStart) && (substr($_GET['Code'], 5, 4) != 'TEST')) {
     header("location: comingsoon.php");
}

else if(time() > strtotime($PromoEnd) && (substr($_GET['Code'], 5, 4) != 'TEST')) {
	header("location: expired.php");
}

if (!function_exists("GetSQLValueString")) {
function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
{
  if (PHP_VERSION < 6) {
    $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
  }

  $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

  switch ($theType) {
    case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "long":
    case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
    case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
    case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
    case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
  }
  return $theValue;
}
}

$today = date('Y-m-d');

$FormCode_CurrentCode = "-1";
if (isset($_GET['Code'])) {
  $FormCode_CurrentCode = $_GET['Code'];
}

try {
	$stmt = $db->prepare("SELECT * FROM $DB_Promo WHERE md5(Promo_ID)=? LIMIT 1");
	$stmt->bindParam(1, $_GET['auth'], PDO::PARAM_STR);
	$stmt->execute();
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$stmt->closeCursor();
		
} catch (PDOException $e) {
	// TODO: proper handling.
	echo "An error occurred:" . $e->getMessage();
}

?>



<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?php echo $promoHost; ?>.com</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
 <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
  <link href='//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link id="elemento-theme" href= "<?php echo $Path ?>assets/css/bootstrap.fire.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="assets/css/bootstrap-material-design.min.css">
  <link rel="stylesheet" href="assets/css/bootstrap-material-datetimepicker.css">
  <link id="elemento-theme" href="assets/css/main.css" rel="stylesheet">
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
  <script src="assets/js/jvfloat.min.js"></script>
  <link id="jvfloat" href="assets/css/jvfloat.css" rel="stylesheet">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  
  
  

<script src="assets/js/material.min.js"></script>
<script src="assets/js/moment-with-locales.min"></script>

  
  <script src='https://www.google.com/recaptcha/api.js'></script>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $Path ?>assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $Path ?>assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $Path ?>assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo $Path ?>assets/ico/apple-touch-icon-57-precomposed.png">
</head>

<body>

<div class="container-fluid heading">
	<div class="row">
		<div class="col-md-12">
			<?php if (file_exists('../assets/img/logo.png')) : ?>
				<img src="../assets/img/logo.png" alt="<?php echo $promoHost; ?>.com">
			<?php else : ?>
				<img src="<?php echo $Path ?>assets/img/logo.png" alt="<?php echo $promoHost; ?>.com">
			<?php endif; ?>
		</div>
	</div>
</div>


<div class="container-fluid content">

  <!-- Example row of columns -->
  <div class="row">
    <div class="col-md-6 left">
	    <h1><?php echo $brandIntroHeading; ?></h1>
		<p><?php echo $brandIntroText; ?></p>
    </div>
    <div class="col-md-6 right">
	    <center>
        	<span class="label label-warning theme" style="font-size:x-large; padding:10px;">Step 3</span>
        </center>
        <div class="progress progress-warning progress-striped active" style="display:block;">
        	<div class="bar theme" style="width: 100%;">Step 3 of 3</div>
        </div>
        <center>
        <?php if (isset($_GET['activation'])) : ?>
			<center>
				<div class='alert alert-success pink fade in'>
					<button type='button' class='close' data-dismiss='alert'>&times;</button>
			        <table width='90%' border='0'>
				        <tr>
					        <td width='16px' valign='top'>
								<i class='icon-success-sign'></i>
							</td>
							<td>
								<strong>Dedicated! We respect that.</strong> Looks like we already received activation for this entry on <?php echo date('l, F j, Y', strtotime($_GET['activation'])); ?> at <?php echo date('g:i a', strtotime($_GET['activation'])); ?> CST. You will receive an email if you are selected as our winner. Please <a href="https://<?php echo $promoHost; ?>.zendesk.com" target="_blank">contact customer support</a> if you have any questions.
							</td>
						</tr>
					</table>
				</div>
			</center>
		<?php endif; ?>
        	<h4>Congratulations! You have successfully entered for a chance to win <?php echo $PromotionArticle; ?> <?php echo $Promotion; ?> courtesy of <?php echo $Brand; ?>. The information below has been stored for this entry. You'll receive an email via the email address below if you are a selected winner. <strong>Be sure to check your junk/spam folder.</strong></h4>
        </center>
        
		<table width="340" border="0">
          <tr>
            <td width="134">&nbsp;</td>
            <td width="196">&nbsp;</td>
            </tr>
          <tr>
            <td height="37">First Name:</td>
            <td><?php echo $row['Fname']; ?></td>
            </tr>
          <tr>
            <td height="38">Last Name:</td>
            <td><?php echo $row['Lname']; ?></td>
            </tr>
          <tr>
            <td height="39">Email:</td>
            <td><?php echo $row['Email']; ?></td>
            </tr>
          <tr>
            <td height="39">Company:</td>
            <td><?php echo $row['Company']; ?></td>
            </tr>
          <tr>
            <td height="39">Phone Number:</td>
            <td><?php echo $row['Phone']; ?></td>
            </tr>
          <tr>
            <td height="39">Promotion Tpye:</td>
            <td><?php echo $row['Promo']; ?></td>
            </tr>
        </table>
	
	</div>
  </div>
  <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<footer>
			    <p>&copy; <?php echo $promoHost; ?>.com <?php date('Y'); ?>. All Rights Reserved. </p>
			    <p>
				    <a href="<?php echo $Path ?>FAQ.php">Frequently Asked Questions</a> | 
				    <a href="https://<?php echo $promoHost; ?>.zendesk.com/hc/en-us">Contact Support</a>
				    <br>
					Use of this website constitutes your acceptance of our <a href="<?php echo $Path ?>tos.php">Terms of Service</a> and <a href="<?php echo $Path ?>pp.php">Privacy Policy</a>.
					<br>
					<small><?php echo $Legal; ?></small>
			    </p>
			</footer>
		</div>
	</div>

</div>
<!-- /container -->

<!-- Le javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<!-- Start of redeemyouroffers Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","redeemyouroffers.zendesk.com");
/*]]>*/</script>
<!-- End of redeemyouroffers Zendesk Widget script -->

</body>
</html>