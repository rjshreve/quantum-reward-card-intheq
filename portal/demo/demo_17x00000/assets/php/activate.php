<?php
	date_default_timezone_set('America/Chicago');
	require_once('variables.php');
	
	require_once('../../../../core/connections/intheq-rewardcard.php');
	
	// Get prize wheel POST variables
	$secureID = $_GET["auth"];
	$currentdate = date('Y-m-d H:i:s');
	
	try {
		$stmt = $db->prepare("SELECT * FROM $DB_Promo WHERE md5(Promo_ID)=? LIMIT 1");
		$stmt->bindParam(1, $secureID, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		$stmt->closeCursor();
			
	} catch (PDOException $e) {
		// TODO: proper handling.
		echo "An error occurred:" . $e->getMessage();
	}
	// comment out while in test
	// As long as our user has redeemed less than three codes, redeem them now
	
	if ($row['Activation'] == NULL) {
	
		try {
			$stmt = $db->prepare("UPDATE $DB_Promo SET Activation=? WHERE md5(Promo_ID)=?");
			$stmt->bindParam(1, $currentdate, PDO::PARAM_STR);
			$stmt->bindParam(2, $secureID, PDO::PARAM_STR);
			$stmt->execute();
			$stmt->closeCursor();
			
			
			$holdstmt = $db->prepare("INSERT INTO $DB_Hold (Fname, Lname, Email, Company, Phone, Promo) VALUES(:Fname, :Lname, :Email, :Company, :Phone, :Promo)");
			$holdstmt->execute(array(':Fname' => $row['Fname'], ':Lname' => $row['Lname'], ':Email' => $row['Email'], ':Company' => $row['Company'], ':Phone' => $row['Phone'], ':Promo' => $row['Promo']));
			$holdstmt->closeCursor();
			header("Location: ../../activatecomplete.php?auth=$secureID");
		
		} catch (PDOException $e) {
			// TODO: proper handling.
			echo "An error occurred:" . $e->getMessage();
		}
	} else {
		$activationDate = $row['Activation'];
		header("Location: ../../activatecomplete.php?auth=$secureID&activation=$activationDate");
	}

?>