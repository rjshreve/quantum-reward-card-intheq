<?php

// Promotion Type (0 = Internal / 1 = External)
$promoType = '1';

// Promotion Host (ThePromoCard / RedeemYourOffers)
$promoHost = 'QuantumRewardCard';

// START Database Information
$DB_Promo = "DEMO_Promo";
$DB_Hold = "DEMO_Hold";

// Virtual Incentives Info for External Promos
$VI_ProgID = "11111";
$VI_itemID = "DEMO";
$VI_itemPrice = "100";
$VI_SKUID = "UVC-V-R18";

$brandIntroHeading = "Thank you for participating in our demo!";
$brandIntroText = "Please complete the redemption process.";

$smsactive = 'true';
$activation = 'true';
$emailSecurityCheck = '3';

// Promotion-specific variables

// Day site opens for redemptions
$PromoStart = "2017/04/18";

// Last day of redemptions
// CODES WILL NOT WORK at 12:00AM ON THIS DATE
$PromoEnd = "2020/12/31";

// Site shuts off
$PromoOff = "2021/01/30";

// This is silly, but needs to stay for the moment.
$Path = "../../core/";

$PreCode = "demo";

// Used to direct users to the proper brand's gateway
$URLBrand = "demo";

// Specific portal for promotion
$URLPromotion = $PreCode."_17x00000";

$PromotionArticle = "a";
// Verbose prize for current promotion
$Promotion = "$100 Prepaid Visa&reg;";

// Verbose brands, used in communication with customers
$Brand = "Quantum Reward Card";
$PrizeBrand = "Visa";

$Theme = "EE8A21";

$Legal = "This Prepaid Visa is issued by Sutton Bank, member FDIC, pursuant to license by Visa U.S.A. Inc. Card powered by Marqeta.";

?>
