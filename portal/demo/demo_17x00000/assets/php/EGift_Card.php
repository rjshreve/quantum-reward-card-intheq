<?php
//============================================================+
// File name   : example_051.php
// Begin       : 2009-04-16
// Last Update : 2011-06-01
//
// Description : Example 051 for TCPDF class
//               Full page background
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Full page background
 * @author Nicola Asuni
 * @since 2009-04-16
 */

require_once('../../../../../codes/tcpdf/config/lang/eng.php');
require_once('../../../../../codes/tcpdf/tcpdf.php');
require_once('variables.php');


// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        //$img_file = K_PATH_IMAGES.'../../../officedepot/redeem/ODLHD_15x01284/assets/img/egc.jpg';
        //$this->Image($img_file, 0, 0, 216, 280, '', '', '', false, 300, '', false, false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        // set the starting point for the page content
        $this->setPageMark();
    }
}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('ThePromoCard.com');
$DocTitle = "Courtesy Prize provided by ".$Brand;
$pdf->SetTitle(htmlentities($DocTitle));
$pdf->SetSubject("Courtesy Prize provided by ".$Brand);
$pdf->SetKeywords("Courtesy Prize provided by ".$Brand);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(0);
$pdf->SetFooterMargin(0);

// remove default footer
$pdf->setPrintFooter(false);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// add a page
$pdf->AddPage();

// Print a text

$fname = $_POST['fname'];
$lname = $_POST['lname'];
$email = $_POST['email'];
$promo = $_POST['promo'];
$prize = $_POST['prize'];
$pin = $_POST['pin'];

$html  = <<<EOD


<p>Dear $fname,</p>
<p><strong>Enjoy your Movie Reward!</strong></p>
<p>Your reward code is: <span style="color:red">$prize</span></p>
  
<ol style="font-size: 10px;">
  <li>Please visit <a href="http://www.activaterewards.com/officedepot" target="_blank">http://www.activaterewards.com/officedepot</a></li>
  <li>Print your movie certificate and present it at the box office at a participating movie theater</li>
</ol>
<p>Thank you!</p>
<p><span style="font-size: 10px">Movie certificate is good towards two admissions (up to $24 total value) to see any movie at participating theaters in the U.S., at any showing including those designated as "no coupons, no passes" in local theater listing guides. Certificate void if not used within 2 months after online code activation. If lost, cannot be replaced. No reproductions will be accepted. No cash value. No change will be provided. Internet access and printer required, and must bring printed certificate to the participating theater box office to redeem. Not valid with any other offer. Offer valid only in U.S. and valid for one-time use only. Not for resale; void if sold or exchanged. If cost of movie admissions is more than maximum value of the movie certificate then user must pay the difference. Limit 1 reward per household/business. Fandango Loyalty Solutions, LLC is not a sponsor or co-sponsor of this program. Movie certificate is powered by Fandango Loyalty Solutions, LLC. ©2015 Fandango Loyalty Solutions, LLC. All Rights Reserved.</span></p>


EOD;

$pdf->writeHTML($html, true, false, true, false, '');


// ---------------------------------------------------------

//Close and output PDF document
$today = date("Y_m_d_H_i_s");
$FileName = $Brand."_Prize_".$today;
$pdf->Output($FileName, 'I');

//============================================================+
// END OF FILE
//============================================================+

?>