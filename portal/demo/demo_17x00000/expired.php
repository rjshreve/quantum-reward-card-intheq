<!DOCTYPE html>

<?php

require_once('assets/php/variables.php');

?>

<html lang="en">
<head>
  <meta charset="utf-8">
  <title><?php echo $promoHost; ?>.com</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Le styles -->
  <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
  <link href='//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
  <link id="elemento-theme" href= "<?php echo $Path ?>assets/css/bootstrap.fire.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel="stylesheet" href="assets/css/bootstrap-material-design.min.css">
  <link rel="stylesheet" href="assets/css/bootstrap-material-datetimepicker.css">
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/themes/smoothness/jquery-ui.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.0/jquery-ui.min.js"></script>
  <script src="assets/js/jvfloat.min.js"></script>
  <link id="jvfloat" href="assets/css/jvfloat.css" rel="stylesheet">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  <script type='text/javascript' src='assets/js/winwheel_1.2.js'></script>
  
  

<script src="assets/js/material.min.js"></script>
<script src="assets/js/moment-with-locales.min"></script>
<script src="assets/js/bootstrap-material-datetimepicker.js"></script>
  
  
  
  <script src='https://www.google.com/recaptcha/api.js'></script>

  <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
  <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <![endif]-->

  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $Path ?>assets/ico/apple-touch-icon-144-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $Path ?>assets/ico/apple-touch-icon-114-precomposed.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $Path ?>assets/ico/apple-touch-icon-72-precomposed.png">
  <link rel="apple-touch-icon-precomposed" href="<?php echo $Path ?>assets/ico/apple-touch-icon-57-precomposed.png">
  <link id="elemento-theme" href="assets/css/main.css" rel="stylesheet">
  
</head>

<body>

<div class="container-fluid heading">
	<div class="row">
		<div class="col-md-12">
			<div class="col-md-10 col-md-push-1 text-center">
				<?php if (file_exists('/assets/img/logo.png')) : ?>
					<img src="/assets/img/logo.png" alt="<?php echo $promoHost; ?>.com">
				<?php else : ?>
					<img src="<?php echo $Path ?>assets/img/promocardlogo.png" alt="<?php echo $promoHost; ?>.com">
				<?php endif; ?>
			</div>
			<div class="col-md-1">
			</div>
		</div>
	</div>
</div>


<div class="container-fluid content">
	
	

  <!-- Example row of columns -->
  
  <div class="row">
    <div class="col-md-5 left">
	   <h1><?php echo $brandIntroHeading; ?></h1>
	   <p><?php echo $brandIntroText; ?></p>
    </div>
    <div class="col-md-6 col-md-push-1 right">
	    <div id="wheel_wrapper">
		    <div class="">
		        <div class="spinning_wheel">
			        
			        <p>We’re sorry, but The <?php echo $Brand; ?> <?php echo $Promotion; ?> promotion has finished. We hope you had a chance to participate and please reach out to our support team with any questions.</p>
					
			        </p>
			        
		        </div>
		    </div>
		</div>
	
	</div>
  </div>
  <div class="container-fluid" style="margin-top: 50px;">
	<div class="row">
		<div class="col-md-12">
			<footer>
			    <p>&copy; <?php echo $promoHost; ?>.com <?php date('Y'); ?>. All Rights Reserved. </p>
			    <p class="small"><?php echo $Legal; ?></p>
			    <p>

				    <a href="https://<?php echo $promoHost; ?>.zendesk.com/hc/en-us">Contact Support</a>
				    <br>
					Use of this website constitutes your acceptance of our <a href="<?php echo $Path ?>tos.php">Terms of Service</a> and <a href="<?php echo $Path ?>pp.php">Privacy Policy</a>.
					<br>
			    </p>
			</footer>
		</div>
	</div>

</div>
<!-- /container -->

<!-- Le javascript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<!-- Start of redeemyouroffers Zendesk Widget script -->
<script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","redeemyouroffers.zendesk.com");
/*]]>*/</script>
<!-- End of redeemyouroffers Zendesk Widget script -->

</body>
</html>