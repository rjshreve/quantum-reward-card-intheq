<?php

if(isset($_GET['Code'])) {
	$PromoCode = strtoupper(filter_var(preg_replace('/[^A-Za-z0-9\-@.]/', '', $_GET['Code']), FILTER_SANITIZE_SPECIAL_CHARS));
} else {
	$PromoCode = strtoupper(filter_var(preg_replace('/[^A-Za-z0-9\-@.]/', '', $_POST['PromoCode']), FILTER_SANITIZE_SPECIAL_CHARS));
}

if(isset($PromoCode) && substr($PromoCode, 0, 5) == 'DEMO1'){
 header("Location: https://portal.quantumrewardcard.com/demo/demo_17x00000/index.php?Code=$PromoCode");
} else {
header("Location: https://portal.quantumrewardcard.com/demo/demo_17x00000/index.php?error=1&Code=$PromoCode");
}
?>
